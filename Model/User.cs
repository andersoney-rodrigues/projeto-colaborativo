using System;
using System.Diagnostics.CodeAnalysis;
namespace ProjetoColaborativo.Models
{
    public class User
    {
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public DateTime created_date { get; set; }
        public DateTime updated_date { get; set; }
    }
}