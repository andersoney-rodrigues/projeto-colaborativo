using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Data.SqlClient;
using System.Text;

namespace ProjetoColaborativo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(new DateTime());
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = "http://127.0.0.1:3307";
                builder.UserID = "dotnet";
                builder.Password = "password";
                builder.InitialCatalog = "dotnet";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    Console.WriteLine("\nQuery data example:");
                    Console.WriteLine("=========================================\n");

                    connection.Open();

                    //         String sql = "SELECT * FROM sys.databases";

                    //         using (SqlCommand command = new SqlCommand(sql, connection))
                    //         {
                    //             using (SqlDataReader reader = command.ExecuteReader())
                    //             {
                    //                 while (reader.Read())
                    //                 {
                    //                     Console.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                    //                 }
                    //             }
                    //         }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
